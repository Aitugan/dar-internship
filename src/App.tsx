import React from 'react';
import './App.scss';

import Header from './components/header/Header';
import MainNav from './components/mainNav/MainNav'

const App: React.FC = () => {


  return (
    <div className="App">
      <Header />
      <MainNav />
    </div>
  );
}

export default App;
