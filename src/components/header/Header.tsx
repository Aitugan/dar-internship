import React from 'react';
import './Header.scss';
import logo from '../../logo.svg';

const Header: React.FC = () => {
    return (
        <div className="Header">
        <header>
          <img src={logo} alt="logo" className="logo" />
          <nav className="header-nav">
            <ul>
                <li>
                <a href="/">Home</a>
                </li>
                <li>
                <a href="/">Main</a>
                </li>
                <li>
                <a href="/">About</a>
                </li>
                <li>
                <a href="/">Contacts</a>
                </li>
                <li>
                <a href="/">Map</a>
                </li>
            </ul>
          </nav>
          <button>Log in</button>
        </header>
      </div>
  
    );
}

export default Header;