import React from 'react';
import './MainNav.scss';

const MainNav: React.FC = () => {
    return (
        <div className="MainNav">
        <nav className="main-nav">
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/">Main</a>
            </li>
            <li>
              <a href="/">About</a>
            </li>
            <li>
              <a href="/">Contacts</a>
            </li>
            <li>
              <a href="/">Map</a>
            </li>
          </ul>
        </nav>
      </div>
  
    );
}

export default MainNav;